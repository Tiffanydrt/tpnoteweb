<?php

function recupererDinos()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    $dinos = json_decode($response->body);
    return $dinos;
}

function dinosArecuperer($slug)
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/'.$slug);
    $dinos = json_decode($response->body);
    return $dinos;
}

function dinosAleatoire()
{
    $dinos = recupererDinos();
    $random_keys = array_rand($dinos, 3); // On récupére trois clés aléatoires
    $top_dinos = array();
    for ($compteur = 0; $compteur <3; $compteur++)
    {
        $top_dinos[]=$dinos[$random_keys[$compteur]];
    }
    return $top_dinos;
}




