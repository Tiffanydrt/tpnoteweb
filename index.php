
<?php
require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

//Indique a flight qu'on utilise Twig en extension 
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/', function(){
    $data = [
        "dinos" => recupererDinos(),
    ];
    Flight::render('dinos.twig', $data);
});

Flight::route('/dino/@slug/', function($slug){
    
    $data = [
        'dinos' => dinosArecuperer($slug),
        'dinosAleatoire' => dinosAleatoire(),
    ];

    Flight::render('detaildinosaur.twig', $data); 

});


Flight::start(); 

