<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

   

    public function test_liste_dinos()
    {
        $response = $this->make_request("GET", "/");
        $dino = recupererDinos();
        $this->assertEquals(200, $response->getStatusCode());
        $body = $response->getBody()->getContents();
        foreach($dino as $dinos){
            $this->assertStringContainsString($dino->avatar, $body);
            $this->assertStringContainsString($dino->name, $body);
    }
    }
    public function test_detail()
    {
        $response = $this->make_request("GET", "/dinosaur/@dinoslug");
        $this->assertEquals(200, $response->getStatusCode());
       // $this->assertEquals("Hello World!", $response->getBody()->getContents());
       // $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

    
}