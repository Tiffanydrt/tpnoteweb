<?php require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase {


    public function test_recupererDinos(){
        $dinos = recupererDinos();
        $this->assertIsArray($dinos);
        $this->assertIsString($dinos[0]->avatar);
    }

    public function test_renderHTMLFromMarkdown()
    {
        $this->assertEquals("<h2>Test</h2>", renderHTMLFromMarkdown("## Test"));
    }


    public function test_readFileContent()
    {
        $this->assertEquals("## Test", readFileContent("pages/test.md"));

        $this->expectException(Exception::class);
        readFileContent("another_file");
    }

    public function test_getPageContent()
    {
        $this->assertEquals("## Test", getPageContent("test"));
    }
}
